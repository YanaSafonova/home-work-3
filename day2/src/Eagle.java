public class Eagle extends Carnivorous implements Sleep {
    private boolean sleeping = false;
    @Override
    public void sleep() {
        this.sleeping = true;
    }
    @Override
    public void wakeUp() {
        this.sleeping = false;
    }
    @Override
    public void eat(Food food) {
        if(!this.sleeping)
            super.eat(food);
        else System.out.println("Я не могу есть, я сплю!");
    }

    @Override
    public void report() {
        if(this.sleeping)
            System.out.println("Наелся и спит");
        else
            System.out.println("Бодр и готов на свершения!");
    }

    public Eagle(String name){
        super(name);
    }
    @Override
    public void voice() {
        System.out.println("Курлычет по-орлиному");
    }
}
