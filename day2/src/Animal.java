public abstract class Animal {
    protected boolean hungry = true;
    public String name;
    public Animal(String name){
        this.name = name;
    }
    public void getHungry(){
        if(this.hungry)
            System.out.println("Голоден!");
        else System.out.println("Покушал:)");
    }
    public abstract void eat(Food food);
    public abstract void voice();
}
