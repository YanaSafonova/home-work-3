public class Tiger extends Carnivorous implements Thirsty{
    private boolean thirsty = true;

    @Override
    public void drink() {
        this.thirsty = false;
    }

    @Override
    public void toilet() {
        this.thirsty = true;
    }

    @Override
    public void report() {
        if(thirsty)
            System.out.println("Хочу пить");
        else
            System.out.println("Хочу в туалет");
    }

    public Tiger(String name){
        super(name);
    }
    @Override
    public void voice() {
        System.out.println("Ррррррррарррр");
    }
}

