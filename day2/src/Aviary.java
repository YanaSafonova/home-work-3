import java.util.HashMap;

public class Aviary <T extends Animal>{
    private int size;
    private int count = 0;
    private HashMap<String,T> animals = new HashMap<>();
    public Aviary(int size){
        this.size = size;
    }
    public int getSize(){
        return this.size;
    }
    public void addAnimal(T animal){
        if(this.count >= this.getSize())
            System.out.println("Вольер переполнен!");
        else {
            this.count++;
            animals.putIfAbsent(animal.name,animal);
        }
    }
    public void deleteAnimal(String name){
        if(animals.containsKey(name))
        animals.remove(name);
        else System.out.println("Животного с такой кличкой нет!");
    }
    public T getAnimal(String name){
        if(!animals.containsKey(name))
            System.out.println("Животного с такой кличкой нет!");
            return animals.get(name);

    }
}
