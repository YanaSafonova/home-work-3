public interface Sleep {
    public void sleep();
    public void wakeUp();
    public void report();
}
