public abstract class Carnivorous extends Animal{
    public Carnivorous(String name){
        super(name);
    }
    @Override
    public void eat(Food food) {
        if(food instanceof Meat) {
            this.hungry = false;
        }
        else
            System.out.println("Я такое не ем!");
    }
}
