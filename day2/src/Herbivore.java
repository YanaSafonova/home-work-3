public abstract class Herbivore extends Animal{
    public Herbivore(String name){
        super(name);
    }
    @Override
    public void eat(Food food) {
        if(food instanceof Grass) {
            this.hungry = false;
        }
        else
            System.out.println("Я такое не ем!");
    }
}
