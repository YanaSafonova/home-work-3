import java.util.ArrayList;

public class Zoo {
    public static int sizeCarn = 3;
    public static int sizeHerb = 5;
    public Aviary<Carnivorous> aviaryCarn ;
    public Aviary<Herbivore> aviaryHerb;
    public Zoo(){
        this.aviaryCarn = new Aviary(sizeCarn);
        this.aviaryHerb = new Aviary(sizeHerb);
    }
    public static void main(String[] args) {
        Zoo zoo = new Zoo();
        Meat meat = new Meat();
        Grass grass = new Grass();
        Lion lion = new Lion("Симба");
        Eagle eagle = new Eagle("Зазу");
        Tiger tiger = new Tiger("Шерхан");
        Duck duck = new Duck("Дональд");
        Lemur lemur = new Lemur("Джулиан");
        Sheep sheep = new Sheep("Шон");


        lion.eat(grass);
        lion.eat(meat);
        lion.getHungry();
        lion.play();

        zoo.aviaryCarn.addAnimal(lion);
        zoo.aviaryCarn.addAnimal(tiger);
        zoo.aviaryCarn.addAnimal(eagle);

        zoo.aviaryCarn.addAnimal(lion);// сообщение о переполнении вольера

        duck.sleep();
        duck.eat(grass);

      zoo.aviaryHerb.addAnimal(duck);
      zoo.aviaryHerb.addAnimal(sheep);
      zoo.aviaryHerb.addAnimal(lemur);

zoo.aviaryCarn.getAnimal("Симба").voice();
zoo.aviaryHerb.getAnimal("Шон").voice();
zoo.aviaryHerb.deleteAnimal("Шон");
zoo.aviaryCarn.getAnimal("Шон");

    }
}
